import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AlertController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { TransactionSucceededPage } from '../transaction-succeeded/transaction-succeeded';
/**
 * Generated class for the PurchasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-purchase',
  templateUrl: 'purchase.html',
})
export class PurchasePage {
  qrData = 11;
  createdCode = null;
  scannedCode = null;
  constructor(public http:HTTP,private alertCtrl: AlertController,private barcodeScanner: BarcodeScanner,public navCtrl: NavController, public navParams: NavParams, private qrScanner: QRScanner) {
  }
   createCode() {
    this.createdCode = this.qrData;
  }
    scanQR(){
    this.barcodeScanner.scan().then(barcodeData => {
    console.log('Barcode data', barcodeData);
       
   var da = JSON.parse(barcodeData.text);
 this.http.post('http://18.217.229.46/user.php', {"action": "make_transaction", "cid": 5, "sid": 6, "amount": da.amount}, {})
  .then(data => {
    
    this.navCtrl.push(TransactionSucceededPage,{"amount": da.amount});
  })
  .catch(error => {

    console.log(error.status);
    console.log(error.error); // error message as string
    console.log(error.headers);

  });

}).catch(err => {
    console.log('Error', err);
});
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad PurchasePage');
  }
 showCamera() {
  (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
}

hideCamera() {
  (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
}
ionViewWillEnter(){
   this.showCamera();
}
ionViewWillLeave(){
   this.hideCamera(); 
} 
}
