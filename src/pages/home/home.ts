import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PurchasePage} from "./../purchase/purchase";
import { HTTP } from '@ionic-native/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AlertController } from 'ionic-angular';
import { TransactionSucceededPage } from '../transaction-succeeded/transaction-succeeded';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  balance = this.navParams.get("balance");
  secret = this.navParams.get("secret");
  cid = this.navParams.get("id");
  qrData = 11;
  createdCode = null;
  scannedCode = null;
  constructor(private alertCtrl: AlertController,private barcodeScanner: BarcodeScanner,public navCtrl: NavController, public navParams: NavParams,private http: HTTP) {
  this.getData();
   this.balance = this.navParams.get("balance");
   this.secret = this.navParams.get("secret");
   this.cid = this.navParams.get("id");
} 
  
    scanQR(){
    this.barcodeScanner.scan().then(barcodeData => {
    console.log('Barcode data', barcodeData);
    this.cid = this.navParams.get("id");   
   var da = JSON.parse(barcodeData.text);
   console.log(da.amount);
   console.log(da.sid);
   console.log(this.cid);
 this.http.post('http://18.217.229.46/user.php', {"action": "make_transaction", "cid": this.cid, "sid": da.sid, "amount": da.amount}, {})
  .then(data => {
    
    this.navCtrl.push(TransactionSucceededPage,{"amount": da.amount});
  })
  .catch(error => {

    console.log(error.status);
    console.log(error.error); // error message as string
    console.log(error.headers);

  });

}).catch(err => {
    console.log('Error', err);
});
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.getData();
    
  }
  ionViewDidEnter(){
    console.log('view did enter');
    this.getData();
  }
  ionViewWillEnter(){
    console.log('view will enter');
    this.getData();
  }
  purchase() {
    console.log('ddd');
    // this.navCtrl.push(PurchasePage,{});
    this.scanQR();
  }
  getData(){
    this.http.get('http://18.217.229.46/user.php?action=get_user&uid=5', {}, {})
  .then(data => { 
    // this.balance = data.data.data[0].balance;
    var d = JSON.parse(data.data);
    // var s = JSON.parse(d);
    this.balance = d.data.BALANCE;
    this.secret = d.data.secret;
    console.log(d.data);
    console.log(d.data.BALANCE);
    // console.log(s);
    // this.balance = d.data.balance;
    // console.log(data.status);
    // console.log(data.data); // data received by server
    // console.log(data.headers);

  })
  .catch(error => {

    console.log(error.status);
    console.log(error.error); // error message as string
    console.log(error.headers);

  });
  }

}

