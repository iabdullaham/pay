import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionSucceededPage } from './transaction-succeeded';

@NgModule({
  declarations: [
    TransactionSucceededPage,
  ],
  imports: [
    IonicPageModule.forChild(TransactionSucceededPage),
  ],
})
export class TransactionSucceededPageModule {}
