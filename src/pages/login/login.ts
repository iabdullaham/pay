import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { HomePage } from '../home/home';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
username:string = "";
password:string = "";
  constructor(public http:HTTP,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

login(){
  console.log(this.username);
  console.log(this.password);
  if(this.username === "" || this.password === ""){
    console.log("naaah");
    
  }else{
   console.log("good job!");
   this.http.post('http://18.217.229.46/user.php', {"action": "login", "phone": this.username, "password": this.password}, {})
  .then(data => {
      var da = JSON.parse(data.data);
    this.navCtrl.push(HomePage,{"balance": da.data.BALANCE, "secret": da.data.secret,"id": da.data.id});
  })
  .catch(error => {

    console.log(error.status);
    console.log(error.error); // error message as string
    console.log(error.headers);

  });
  }
}
}
